Classifier:

-The data set used is: https://drive.google.com/open?id=1i13O6NZLgwOti7os5bP2wrlwcG6YH4yI

-Please refer the Birds Number_name.csv or Birds Number_name.pdf for corresponding name and number list

-All the functions have flags to calculate or create files
    parse() in parser.py =>
        plot = visualise the raw signal in .png format
        save = save .h5 file of main audio file
        convert_to_PCM24 = convert all audio files to PCM 24 for processing (Should be true for the first time)

    segment() in mathfunctions =>
        plot_env = Plot Spectrogram
        plot_spec = Plot Energy Envelope
        plot_resp = Plot Frequency Response

    manualsegmentation in manualseg =>
        generatewavefile = Generate individual syllable wave files
        generatemathplots = Plot Frequency response

-The syllable files created are in the following fashion =>
    path: out/syllables (Path can be modified according to convenience)

    format: A_B_C.wav ( eg. 12_0_3.wav )
        A: Bird number (eg. 12- Avadavat 12 Red avadavat)
        B: Bird Call (Some birds in the dataset have multiple calls. If a bird has 2 calls then- 0 indicates first call and 1 indicates second call)
        C: Syllable Number (The number of syllable of that call)