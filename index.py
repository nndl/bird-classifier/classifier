import wave
import os   
from sklearn import naive_bayes,svm,tree
from sklearn.svm import NuSVC
from sklearn.naive_bayes import BernoulliNB
from sklearn.metrics import accuracy_score
from sklearn.manifold import TSNE
from matplotlib import pyplot as plt
import matplotlib.cm as cm
import numpy as np
from lib import utils, parser, autoseg, manualseg,timestamps,featurextraction,datavisualisation,mathfunctions

ROOT = 'data'
files = utils.get_files(ROOT, ['wav'])
idx = [None]*len(files)
np.shape(idx)
Training = []
Target = []
Testing = []
Target_test = []
mfcc = []
features = []
for i, f in enumerate(files):
    indices = utils.extract_ints(f)
    index = str(indices[0])+'_'+str(indices[1])
    idx[i] = [index, f]
for entry in idx:
    try:
        index = entry[0]
        filepath = entry[1]
        temp=index.split('_')
        Bird=temp[0]
        raw_signal, rate, sample_width = parser.parse(index, filepath, root_dir='out', plot=False, save=True,convert_to_PCM24=True)
        freqs=mathfunctions.segment(index,raw_signal,rate,sample_width,root_dir='out',plot_env=False,plot_spec=False,plot_resp=False)
        timestamp,time_flag,syllable=timestamps.timestamp(index)
        if time_flag:
            syllablepaths=manualseg.manualsegmentation(Bird,raw_signal,rate,sample_width,syllable,np.array(timestamp),wavpath='out/syllables',generatewavefile=True,generatemathplots=False)
            for i,syllablepath in enumerate(syllablepaths):
                mfcc_feat,features=featurextraction.spectralfeatures(syllablepath,rate,sample_width,write_to_h5=False,read_from_h5=False)
                mfcc = np.append(mfcc,np.transpose(mfcc_feat))
                Training = np.append(Training,np.transpose(features))
                Target = np.append(Target,Bird+'_'+str(syllable))
    except wave.Error as e:
        print('[Error] for bird: '+index)
        # TODO: print stack trace instead of this
        print('Exception: ' + str(e))
        raise e
    except ValueError as e:
        print('[Error] for bird: '+index)
        # TODO: print stack trace instead of this
        # print('Exception: ' + str(e))
        raise e

#Splitting to 2D
print(np.shape(Training))
Training=utils.split_list(Training,len(Target))
print(np.shape(Training))
mfcc=utils.split_list(mfcc,len(Target))

#Normalise Datapoints
Training = utils.normalise_for_zero_mean_unity_variance(Training)

#Joining MFCC and Other Features
Training = np.append(Training, mfcc, axis=1)

print(np.shape(Training))
print(Target)
Training,Target = utils.shuffle_in_unison(Training,Target)
Training,Testing = utils.split_dataset(Training,0.8)
Target,Target_test = utils.split_dataset(Target,0.8)

#clf=NuSVC.

#PLot tSNE
datavisualisation.plot_tsne(Training,Target,tsneflag=True)
