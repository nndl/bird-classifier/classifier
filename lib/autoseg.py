import numpy as np
import os

from numpy.lib import stride_tricks
from matplotlib import pyplot as plt


def segment(index, raw_signal, rate, sample_width, cutoff_frac=0.45, root_dir='out/trial', plot_spec=False, plot_env=False, plot_resp=False):
    spectrogram = stft(raw_signal)
    spectrogram_log, mean_freqs = logscale_spec(spectrogram, rate=rate)
    envelope = energy_envelope(spectrogram_log)
    freq_res, freq_vals = frequency_response(spectrogram, rate)
    delta_freq_res, minimas, maximas = analyse_frequency_response(freq_res)
    min_indices = np.where(minimas == 1)[0]
    max_indices = np.where(maximas == 1)[0]
    upper = int(np.ceil(cutoff_frac*len(freq_res)))
    lower = 0
    if min_indices[0] < max_indices[0]:
        lower = min_indices[0]
    cutoff_freqs = [freq_vals[lower], freq_vals[upper+1]]
    if plot_spec:
        plot_spectrogram(spectrogram_log, mean_freqs, len(raw_signal), img_path=os.path.join(root_dir, index, 'spectrogram.png'))
    if plot_env:
        plot_energy_envelope(envelope, img_path=os.path.join(root_dir, index, 'energy_hanning.png'))
    if plot_resp:
        plot_frequency_response(freq_res, delta_freq_res, minimas, maximas, img_path=os.path.join(root_dir, index, 'frequency_response.png'))
    return cutoff_freqs

def stft(raw_signal, frameSize=144, overlapFac=0.5, window=np.hanning):
    """ short time fourier transform of audio signal """
    win = window if isinstance(window, np.ndarray) else window(frameSize)
    hop_size = int(frameSize - np.floor(overlapFac * frameSize))

    # zeros at beginning (thus center of 1st window should be for sample nr. 0)
    samples = np.append(np.zeros(int(np.floor(frameSize/2.0))), raw_signal)
    # cols for windowing
    cols = int(np.ceil((len(samples) - frameSize) / float(hop_size)) + 1)
    # zeros at end (thus samples can be fully covered by frames)
    samples = np.append(samples, np.zeros(frameSize))

    frames = stride_tricks.as_strided(samples, shape=(cols, frameSize), strides=(
        samples.strides[0]*hop_size, samples.strides[0])).copy()
    frames *= win
    return np.fft.rfft(frames)

def get_frquencies(spec, rate):
    """ give the frequency values for a spectrogram """
    _, freqbins = np.shape(spec)
    return np.abs(np.fft.fftfreq(freqbins*2, 1./rate)[:freqbins+1])

def logscale_spec(spec, rate=48000, factor=20.):
    """ take a stft as input, creates bins on frequency axis 
        logarithmically & sums all the stft coeff's in each bin
        returns this sum (over each freq bin) & mean freq in each bin 
    """
    timebins, freqbins = np.shape(spec)

    scale = np.linspace(0, 1, freqbins) ** factor
    scale *= (freqbins-1)/max(scale)
    scale = np.unique(np.round(scale)).astype(int)

    # create spectrogram with new freq bins
    newspec = np.complex128(np.zeros([timebins, len(scale)]))
    for i in range(0, len(scale)):
        if i == len(scale)-1:
            newspec[:, i] = np.sum(spec[:, scale[i]:], axis=1)
        else:
            newspec[:, i] = np.sum(spec[:, scale[i]:scale[i+1]], axis=1)

    # list center freq of bins
    allfreqs = get_frquencies(spec, rate)
    freqs = []
    for i in range(0, len(scale)):
        if i == len(scale)-1:
            freqs += [np.mean(allfreqs[scale[i]:])]
        else:
            freqs += [np.mean(allfreqs[scale[i]:scale[i+1]])]
    return newspec, freqs

def energy_envelope(spec_log):
    """ compute energy envelope
        input: sum of stft over (logarithmically distributed) freq bins - as a function of time
        output: energy - as a function of time
    """
    ims = 20.*np.log10(np.abs(spec_log)/10e-6)  # amplitude to decibel
    times, _ = np.shape(ims)
    env = np.zeros(times)
    for i in range(times):
        env[i] = np.sum(ims[i, :])
    return env

def frequency_response(spec, rate):
    """ return the frequency response for a spectrogram
        input: spectrogram
        outputs: 
            - log-log distribution of #hits per frequency bin
            - array of frequency bin boundaries
    """
    freqs = get_frquencies(spec, rate)
    hits = np.log(np.sum(np.abs(spec), 0))
    return hits, freqs

def analyse_frequency_response(freq_res):
    """ input: frequency_response
        outputs: 
            deltas(frequency_response): diff of consecutive values
            minimas(frequency_response): bool array 1 => min point, 0 => reg point
            maximas(frequency_response): bool array 1 => max point, 0 => reg point
    """
    deltas = np.diff(freq_res, n=1)
    minimas = np.zeros(len(deltas), dtype=int)
    maximas = np.zeros(len(deltas), dtype=int)
    for i, _ in enumerate(deltas):
        if i == 0:
            maximas[i]=minimas[i]=0
        else:
            if (deltas[i] > 0) & (deltas[i-1] < 0):
                minimas[i] = 1
            elif (deltas[i] < 0) & (deltas[i-1] > 0):
                maximas[i] = 1
            else:
                pass
    np.pad(deltas, (1, 0), 'edge')
    np.pad(minimas, (1, 0), 'edge')
    np.pad(maximas, (1, 0), 'edge')
    deltas[0]=None
    minimas[0]=maximas[0]=0
    return deltas, minimas, maximas

def plot_spectrogram(spec_log, freqs, n_samples, rate=48000, binsize=2**10, img_path=None, colormap="jet"):
    """ inputs: 
            1. sum of stft over (logarithmically distributed) freq bins
            2. freq bins
            3. number of samples in the original signal
            4. sampling freq
    """
    numpy.set_printoptions(threshold=numpy.nan)
    ims = 20.*np.log10(np.abs(spec_log)/10e-6)  # amplitude to decibel

    timebins, freqbins = np.shape(ims)

    plt.figure(figsize=(15, 7.5))
    plt.imshow(np.transpose(ims), origin="lower", aspect="auto",
               cmap=colormap, interpolation="none")
    plt.colorbar()
    
    plt.xlabel("time (s)")
    plt.ylabel("frequency (hz)")
    plt.xlim([0, timebins-1])
    plt.ylim([0, freqbins])

    xlocs = np.float32(np.linspace(0, timebins-1, 5))
    plt.xticks(xlocs, ["%.02f" % l for l in (
        (xlocs*n_samples/timebins)+(0.5*binsize))/rate])
    ylocs = np.int16(np.round(np.linspace(0, freqbins-1, 10)))
    plt.yticks(ylocs, ["%.02f" % freqs[i] for i in ylocs])
    
    if img_path is None:
        plt.show()
    elif (img_path is not None) & (not os.path.isfile(img_path)):
        dir_path = os.path.split(img_path)[0]
        os.makedirs(dir_path, exist_ok=True)
        plt.savefig(img_path, dpi=300)
    else:
        # TODO: implement overwrite flag
        pass
    plt.clf()

def plot_energy_envelope(envelope, img_path=None):
    """ plot energy envelope """
    plt.figure(figsize=(30, 4))
    shape = np.shape(envelope)
    times = np.arange(shape[0])
    plt.plot(times, envelope, color='k', linewidth=0.2)
    plt.xlim(times[0], times[-1])
    plt.xlabel('time (ms)')
    plt.ylabel('energy')
    mean = np.mean(envelope)
    median = np.median(envelope)
    plt.hlines(mean, times[0], times[-1], colors='r', label='mean')
    plt.hlines(median, times[0], times[-1], colors='g', label='median')
    if img_path is None:
        plt.show()
    elif (img_path is not None) & (not os.path.isfile(img_path)):
        dir_path = os.path.split(img_path)[0]
        os.makedirs(dir_path, exist_ok=True)
        plt.savefig(img_path, dpi=300)
    else:
        # TODO: implement overwrite flag
        pass
    plt.clf()

def plot_frequency_response(freq_res, delta_freq_res, minimas, maximas, img_path=None):
    # calculate min & max indices
    min_indices = np.where(minimas == 1)[0]
    max_indices = np.where(maximas == 1)[0]
    f = plt.figure(figsize=(15, 22.5))
    # plot hits (log-log)
    ax1 = f.add_subplot(311)
    plt.title('log-log')
    plt.xlabel('frequency bins')
    plt.ylabel('hits')
    plt.xticks(range(len(freq_res)))
    plt.grid()
    ax1.plot(freq_res, color='k', linewidth=1)
    ax1.vlines(min_indices, min(freq_res), max(freq_res), color='r')
    ax1.vlines(max_indices, min(freq_res), max(freq_res), color='g')
    plt.tight_layout()
    # plot hits log-log
    ax2 = f.add_subplot(312)
    plt.title('delta')
    plt.xlabel('frequency bins')
    plt.ylabel('delta(log(hits))')
    plt.xticks(range(len(freq_res)))
    plt.grid()
    ax2.plot(delta_freq_res, color='k', linewidth=1)
    ax2.vlines(min_indices, min(delta_freq_res), max(delta_freq_res), color='r')
    ax2.vlines(max_indices, min(delta_freq_res), max(delta_freq_res), color='g')
    plt.tight_layout()  
    # plot minimas & maximas
    ax3 = f.add_subplot(313)
    plt.title('extremas')
    plt.xlabel('frequency bins')
    plt.ylabel('minmas & maximas')
    plt.xticks(range(len(freq_res)))
    ax3.scatter(min_indices, np.negative(np.ones(len(min_indices), dtype=int)), marker='o', color='r')
    ax3.scatter(max_indices, np.ones(len(max_indices), dtype=int), marker='x', color='g')
    ax3.hlines(0, 0, len(freq_res), color='k', linewidth=0.2)
    ax3.hlines(1, 0, len(freq_res), color='k', linewidth=0.2)
    ax3.hlines(-1, 0, len(freq_res), color='k', linewidth=0.2)
    ax3.annotate('maximas', xy=(0, 1), ha="right")
    ax3.annotate('minimas', xy=(0, -1), ha="right")
    plt.tight_layout()
    if img_path is None:
        plt.show()
    elif (img_path is not None) & (not os.path.isfile(img_path)):
        dir_path = os.path.split(img_path)[0]
        os.makedirs(dir_path, exist_ok=True)
        plt.savefig(img_path, dpi=300)
    else:
        # TODO: implement overwrite flag
        pass
    plt.clf()
