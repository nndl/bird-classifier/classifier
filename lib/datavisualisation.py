import numpy as np
from sklearn.manifold import TSNE
import matplotlib.cm as cm
from matplotlib import pyplot as plt


def plot_tsne(X,Y,tsneflag=False):
    #The function plots a scater plot for X and Y in variable colors
    if tsneflag:
        labels = set(Y)
        print(labels)
        print(len(labels))

        tsne = TSNE(learning_rate=500)
        X_2d = tsne.fit_transform(X)
        colors = cm.rainbow(np.linspace(0, 1, len(labels)))
        fig = plt.figure()
        plot = fig.add_subplot(111)
        ax = plt.gca()

        for i, (y, c) in enumerate(zip(labels, colors)):
            indices = np.where(Y == y)[0]
            plot.plot(X_2d[np.array(indices), 0], X_2d[np.array(indices), 1], color=c, label=y, marker='o', linewidth=0)
        plt.legend(labels)
        plt.savefig('out/tSNE.png')
        plt.show()