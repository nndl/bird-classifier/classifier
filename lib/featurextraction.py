import os
import librosa
import h5py
import python_speech_features
import numpy as np
from lib import mathfunctions
from matplotlib import pyplot as plt


def spectralfeatures(signalpath,rate,samplewidth,write_to_h5=True,read_from_h5=True):
    
    y,sr=librosa.load(signalpath)

    #centroid
    centroid = librosa.feature.spectral_centroid(y=y, sr=sr)
    centroid_mean = np.mean(centroid)
    centroid_variance = np.var(centroid)
    
    #Bandwidth
    Bandwidth = librosa.feature.spectral_bandwidth(y=y, sr=sr)
    Bandwidth_mean=np.mean(Bandwidth)
    Bandwidth_variance=np.var(Bandwidth)

    #rolloff
    rolloff = librosa.feature.spectral_rolloff(y=y, sr=sr,roll_percent=0.95)
    rolloff_mean=np.mean(rolloff)
    rolloff_variance=np.var(rolloff)

    #flatness
    FLatness = librosa.feature.spectral_flatness(y=y)
    FLatness_mean = np.mean(FLatness)
    FLatness_variance = np.var(FLatness)

    #energy envelope
    spectrogram=mathfunctions.stft(y)
    spectrogram_log, mean_freqs = mathfunctions.logscale_spec(spectrogram, rate=sr)
    envelope = mathfunctions.energy_envelope(spectrogram_log)
    envelope_mean=np.mean(envelope)
    envelope_variance=np.var(envelope)

    #zerpcrossingrate
    zerocrossing = librosa.feature.zero_crossing_rate(y=y)[0]
    zerocrossing_mean=np.mean(zerocrossing)
    zerocrossing_variance=np.var(zerocrossing)


    #MFCC
    mfcc_feat = python_speech_features.base.mfcc(y,sr,nfft=1024,)
    delta_feat = mfcc_feat[:-1]-mfcc_feat[1:]
    deltadelta_feat = delta_feat[:-1]-delta_feat[1:]
    mfcc_feat = mfcc_feat[2:]
    delta_feat = delta_feat[1:]
    mfcc_feat = np.concatenate((mfcc_feat,delta_feat,deltadelta_feat), axis=1)
    mfcc_feat=np.mean(mfcc_feat,axis=0)

    if write_to_h5:
        signalpath=signalpath.split('.w')[0]
        h5f = h5py.File(signalpath+'.h5', 'w')
        h5f.create_dataset('centroid_mean', data=centroid_mean)
        h5f.create_dataset('centroid_variance', data=centroid_variance)
        h5f.create_dataset('Bandwidth_mean', data=Bandwidth_mean)
        h5f.create_dataset('Bandwidth_variance', data=Bandwidth_variance)
        h5f.create_dataset('rolloff_mean', data=rolloff_mean)
        h5f.create_dataset('rolloff_variance', data=rolloff_variance)
        h5f.create_dataset('FLatness_mean', data=FLatness_mean)
        h5f.create_dataset('FLatness_variance', data=FLatness_variance)
        h5f.create_dataset('envelope_mean', data=envelope_mean)
        h5f.create_dataset('envelope_variance', data=envelope_variance)
        h5f.create_dataset('zerocrossing_mean', data=zerocrossing_mean)
        h5f.create_dataset('zerocrossing_variance', data=zerocrossing_variance)
        h5f.create_dataset('MFCC', data=mfcc_feat)
        h5f.create_dataset('Allfeatures', data=[centroid_mean,centroid_variance,Bandwidth_mean,Bandwidth_variance,rolloff_mean,rolloff_variance,FLatness_mean,FLatness_variance,envelope_mean,envelope_variance,zerocrossing_mean,zerocrossing_variance])
        h5f.close()

    if read_from_h5:
        f=h5py.File(signalpath+'.h5','r')
        Feature_list=f['Allfeatures'].value
        MFCC=f['MFCC'].value
        return MFCC,Feature_list
    else:
        return mfcc_feat,[centroid_mean,centroid_variance,Bandwidth_mean,Bandwidth_variance,rolloff_mean,rolloff_variance,FLatness_mean,FLatness_variance,envelope_mean,envelope_variance,zerocrossing_mean,zerocrossing_variance]

    #return centroid,envelope