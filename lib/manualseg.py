import wave
import os
import wavio
import math
import librosa
from lib import parser,mathfunctions

import scipy.io.wavfile
import numpy as np

def manualsegmentation(index,signal,rate,sample_width,syllable,startstoparray,wavpath='out/trial/waves/',generatewavefile=True,generatemathplots=False):
    
    # l1=np.ceil(startstoparray*rate)
    iters=range(len(startstoparray))
    audiopaths = []
    for i in iters:
        start=int(startstoparray[i,0]*rate)
        stop=int(startstoparray[i,1]*rate)
        newsignal=signal[start:stop]
        temp=wavpath + '/' + index + '_' + str(syllable) + '_' + str(i) + '.wav'
        audiopaths=np.append(audiopaths,temp)
        #audiopaths=np.append(audiopaths,(wavpath+ index + '_' +str(syllable) + '_' + str(i) + '.wav'))
        #parser.save_raw_signal(newsignal,rate,sample_width,os.path.join('out/trial',index + '_' +str(syllable) + '_' + str(i) + '.h5'))
        if generatemathplots:
            freqs=mathfunctions.segment(index + '/' +str(syllable) + '_' + str(i),newsignal,rate,sample_width,root_dir=wavpath,plot_env=generatemathplots,plot_resp=generatemathplots,plot_spec=generatemathplots)
        if generatewavefile:
            os.makedirs(wavpath, exist_ok=True)
            print(wavpath)
            wavio.write(wavpath + '/' + index + '_' + str(syllable) + '_' + str(i) + '.wav', newsignal, rate)
    return audiopaths
