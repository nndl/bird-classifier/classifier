import numpy as np
import os
import h5py
import wavio
import wave
import soundfile

from wavio import read as read_wav
from matplotlib import pyplot as plt


def parse(index, path_to_wav_file, root_dir='out/trial', plot=False, save=False,convert_to_PCM24=False):
    raw_signal, rate, sample_width = parse_wav_file(path_to_wav_file,convert_to_PCM24)
    if plot:
        plot_raw_signal(raw_signal, rate, os.path.join(root_dir, index, img_path='raw_signal.png'))
    if save:
        save_raw_signal(raw_signal, rate, sample_width, bin_file_path=os.path.join(root_dir, index, 'raw_signal.h5'))
    return raw_signal, rate, sample_width

def parse_wav_file(path_to_wav_file,convert_to_PCM24):
    """ parese a wav file. returns an signal_as_np_array, rate, sample_width """
    print(path_to_wav_file)
    """Uncomment the 2 line below to convert audio files to 24 bit depth.
        Just need to do it once for wav file"""
    if convert_to_PCM24:
        data,samplerate=soundfile.read(path_to_wav_file)
        soundfile.write(path_to_wav_file,data,samplerate,subtype='PCM_24')
    ans = read_wav(path_to_wav_file)
    raw_signal = np.array(ans.data)
    rate = ans.rate
    sample_width = ans.sampwidth
    return raw_signal, rate, sample_width

def plot_raw_signal(raw_signal, rate, img_path=None):
    """ plot a raw signal. returns nothing """
    times = np.arange(len(raw_signal))/float(rate)
    plt.fill_between(times, raw_signal[:, 0], color='k')
    plt.figure(figsize=(15, 7.5))
    plt.xlim(times[0], times[-1])
    plt.xlabel('time (s)')
    plt.ylabel('amplitude')
    if img_path is None:
        plt.show()
    elif (img_path is not None) & (not os.path.isfile(img_path)):
            dir_path = os.path.split(img_path)[0]
            os.makedirs(dir_path, exist_ok=True)
            plt.savefig(img_path, dpi=300)
    else:
        # TODO: implement overwrite flag
        pass

def save_raw_signal(raw_signal, rate, sample_width, bin_file_path=None):
    """ save a raw signal. returns nothing """
    if bin_file_path is None:
        raise RuntimeError('Pls pass bin_file_path')
    if not os.path.isfile(bin_file_path):
        dir_path = os.path.split(bin_file_path)[0]
        os.makedirs(dir_path, exist_ok=True)
        h5f = h5py.File(bin_file_path, 'w')
        h5f.create_dataset('signal', data=raw_signal)
        h5f.create_dataset('rate', data=rate)
        h5f.create_dataset('sample_width', data=sample_width)
        h5f.close()
