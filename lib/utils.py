from glob import iglob
import itertools as it
import random
from sklearn.utils import shuffle
import numpy as np

X = []

def get_files(root, extentions):
    return list(it.chain.from_iterable(iglob(root+'/**/*.'+ext) for ext in extentions))


def extract_ints(string):
    return [int(s) for s in string.split() if s.isdigit()]

def shuffle_in_unison(a, b):
    #The funstions shuffles the two arrays in the exact same fashion
    rng_state = np.random.get_state()
    np.random.shuffle(a)
    np.random.set_state(rng_state)
    np.random.shuffle(b)
    return a,b

def split_dataset(dataset, splitRatio=0.8):
    #The function splits the Input array into two arrays in the given ratio
	trainSize = int(len(dataset) * splitRatio)
	trainSet = []
	copy = list(dataset)
	while len(trainSet) < trainSize:
		index = random.randrange(len(copy))
		trainSet.append(copy.pop(index))
	return [trainSet, copy]

def split_list(alist, wanted_parts=1):
    #The function converts a 1D array into 2D array by splitting 
    length = len(alist)
    return [ alist[i*length // wanted_parts: (i+1)*length // wanted_parts] 
             for i in range(wanted_parts) ]

def normalise2d(X,top=1,bottom=0):
    #The function returns row wise normalised array of input array between the variables 'top' and 'bottom'
    maxima=np.amax(X,axis=0)
    minima=np.amin(X,axis=0)
    for i in range(len(minima)):
        X[:i] = (top-bottom)*((X[:i] - minima[i])/(maxima[i] - minima[i]))+bottom
    return X

def normalise_for_zero_mean_unity_variance(X):
    mean=np.mean(X,axis=0)
    std_dev=np.sqrt(np.var(X,axis=0))
    mean=np.mean(X, axis=0)
    Y=(X-mean)/std_dev
    return Y